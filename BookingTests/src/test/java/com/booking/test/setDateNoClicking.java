package com.booking.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by roksolana.desyatnyk on 12/8/2016.
 */
public class setDateNoClicking {
    public WebDriver driver;
    String chromeDir = "D:\\Selenium\\ChromeDriver\\chromedriver.exe";
    String url = "http://www.booking.com/";
    String city = "Barcelona";
    String checkInDay = "25";
    String checkInMonth = "10";
    String checkInYear = "2018";
    String checkOutDay = "30";
    String checkOutMonth = "10";
    String checkOutYear = "2018";

    @BeforeClass(alwaysRun = true)
    public void setup(){
        System.setProperty("webdriver.chrome.driver", chromeDir);
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-maximized");
        this.driver = new ChromeDriver(chromeOptions);
    }

    @AfterClass(alwaysRun = true)
    public void teardown(){
        this.driver.quit();
    }

    @Test
    public void loadPage(){
        driver.manage().deleteAllCookies();
        driver.get(url);
    }

    @Test(dependsOnMethods = "loadPage")
    public void enterCity() {
        WebElement cityInput = driver.findElement(By.id("ss"));
        cityInput.sendKeys(city);
        driver.findElement(By.cssSelector(".sb-searchbox__title-text")).click();
    }

    @Test(dependsOnMethods = "enterCity")
    public void enterDates() {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("$('input[name=checkin_month]').attr('value', '" + checkInMonth + "')");
        jse.executeScript("$('input[name=checkin_monthday]').attr('value', '" + checkInDay + "')");
        jse.executeScript("$('input[name=checkin_year]').attr('value', '" + checkInYear + "')");
        jse.executeScript("$('input[name=checkout_month]').attr('value', '" + checkOutMonth + "')");
        jse.executeScript("$('input[name=checkout_monthday]').attr('value', '" + checkOutDay + "')");
        jse.executeScript("$('input[name=checkout_year]').attr('value', '" + checkOutYear + "')");
    }

    @Test(dependsOnMethods = "enterDates")
    public void clickSearchButton() {
        WebElement searchButton = driver.findElement(By.cssSelector(".sb-searchbox__button"));
        searchButton.click();
    }
}