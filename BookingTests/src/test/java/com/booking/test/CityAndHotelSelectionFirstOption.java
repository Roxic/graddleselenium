package com.booking.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by roksolana.desyatnyk on 12/8/2016.
 */
public class CityAndHotelSelectionFirstOption {
    public WebDriver driver;
    String chromeDir = "D:\\Selenium\\ChromeDriver\\chromedriver.exe";
    String url = "http://www.booking.com/";
    String city = "Barcelona";
    String startDate = "29";
    String hotelName = "Apart-Suites Hostemplo";

    @BeforeClass(alwaysRun = true)
    public void setup(){
        System.setProperty("webdriver.chrome.driver", chromeDir);
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-maximized");
        this.driver = new ChromeDriver(chromeOptions);
    }

    @AfterClass(alwaysRun = true)
    public void teardown(){
        this.driver.quit();
    }

    @Test
    public void loadPage(){
        driver.manage().deleteAllCookies();
        driver.get(url);
    }

    @Test(dependsOnMethods = "loadPage")
    public void enterCity() {
        WebElement cityInput = driver.findElement(By.id("ss"));
        cityInput.sendKeys(city);
        driver.findElement(By.cssSelector(".sb-searchbox__title-text")).click();//removes dropdown with city autocomplete
    }

    @Test(dependsOnMethods = "enterCity")
    public void enterCheckInDate() {
        WebElement checkInDate = driver.findElement(By.xpath("//div[contains(@class,'sb-date-field__display') and contains(text(),'Check-in Date')]"));
        checkInDate.click();
        WebElement userCheckInInput = driver.findElement(By.xpath("//span[contains(@class,'c2-day') and contains(text(),'" + startDate + "')]"));
        userCheckInInput.click();
    }

    @Test(dependsOnMethods = "enterCheckInDate")
    public void clickSearchButton() {
        WebElement searchButton = driver.findElement(By.cssSelector(".sb-searchbox__button"));//or ("button[class='sb-searchbox__button'][type='submit']")
        searchButton.click();
    }

    //finding hotel by xPath selector that searches all elements that contain particular hotelName
    @Test(dependsOnMethods = "clickSearchButton")
    public void selectHotel(){
        WebElement searchedHotel = driver.findElement(By.xpath("//span[contains(text(), '" + hotelName + "')]"));
        searchedHotel.click();
    }
}