package com.facebook.tests;

import com.roksi.data.FacebookData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.roksi.utilities.DriverFactory.getBrowserTypeByProperty;
import static com.roksi.utilities.DriverFactory.getDriver;

/**
 * Created by roksolana.desyatnyk on 9/23/2016.
 */
public class Login_Test {

    private WebDriver driver;

    @BeforeClass(alwaysRun = true)
    public void setup(){
        this.driver = getDriver(getBrowserTypeByProperty());

    }

    @AfterClass(alwaysRun = true)
    public void teardown(){
        driver.quit();
    }



    @Test(groups={"p1", "pageLoads"}, dataProvider = "pages", dataProviderClass = FacebookData.class)
    public void loadPage(String url, String title){
        driver.get(url);
        Assert.assertEquals(driver.getTitle(), title);
    }

    @Test(groups={"p2", "field"}, dependsOnMethods = "loadPage")
    public void filloutEmailFld(){
        String email = "test.email@test.com";
        WebElement emailFld = driver.findElement(By.cssSelector("#email"));
        emailFld.sendKeys("test.email@test.com");
        Assert.assertEquals(emailFld.getAttribute("value"), email);
    }

    @Test(groups={"p2", "field"}, dependsOnMethods = "filloutEmailFld")
    public void filloutPassFld(){
        String password = "123456789";
        WebElement passFld = driver.findElement(By.cssSelector("#pass"));
        passFld.sendKeys(password);
        Assert.assertEquals(passFld.getAttribute("value"), password);
    }
}