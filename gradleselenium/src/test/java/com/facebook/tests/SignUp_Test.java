package com.facebook.tests;

import com.roksi.pages.FacebookMainPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.roksi.utilities.DriverFactory.getBrowserTypeByProperty;
import static com.roksi.utilities.DriverFactory.getDriver;

/**
 * Created by roksolana.desyatnyk on 10/20/2016.
 */
public class SignUp_Test {
    public WebDriver driver;
    FacebookMainPage fbMainPage;

    @BeforeClass(alwaysRun = true)
    public void setup(){
        this.driver = getDriver(getBrowserTypeByProperty());
//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        fbMainPage = PageFactory.initElements(driver, FacebookMainPage.class);
    }

    @AfterClass(alwaysRun = true)
    public void teardown(){
        this.driver.quit();
    }

    @Test(groups = {"p1"})
    public void testSignUpMainPage(){
        driver.manage().deleteAllCookies();

        fbMainPage.loadPage();
        fbMainPage.setText_FirstNameField("QA");
        fbMainPage.setText_LasNameField("Roksi");
        fbMainPage.setText_EmailMobileField("penguiniy1@mailinator.com");
        fbMainPage.setText_ReenterEmailMobileField("penguiniy1@mailinator.com");
        fbMainPage.setText_PasswordField("password");
        fbMainPage.selectBdayMonth("8");
        fbMainPage.selectBdayDay("20");
        fbMainPage.selectBdayYear("1990");
        fbMainPage.click_MaleRadio();
        fbMainPage.click_SignUpButton();

//        try {
//            Thread.sleep(10000L);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
}
