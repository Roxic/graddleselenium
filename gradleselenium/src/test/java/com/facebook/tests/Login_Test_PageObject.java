package com.facebook.tests;

import com.roksi.data.FacebookData;
import com.roksi.pages.FacebookLoginPage;
import com.roksi.pages.FacebookMainFeed;
import com.roksi.pages.FacebookMainPage;
import com.roksi.utilities.MyTestListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.roksi.utilities.DriverFactory.*;
import static org.testng.Assert.*;

/**
 * Created by roksolana.desyatnyk on 10/18/2016.
 */
@Listeners(MyTestListener.class)
public class Login_Test_PageObject {

    private WebDriver driver;
    private FacebookMainPage fbMainPage;
    private FacebookLoginPage fbLoginPage;
    private FacebookMainFeed fbMainFeed;

    @BeforeClass(alwaysRun = true)
    public void setup(){
        this.driver = getDriver( getBrowserTypeByProperty() );
//        Changing initialization due to PageFactory pattern
//        fbMainPage = new FacebookMainPage(driver);
        fbMainPage = PageFactory.initElements(driver, FacebookMainPage.class);
        fbLoginPage = PageFactory.initElements(driver, FacebookLoginPage.class);
        fbMainFeed = PageFactory.initElements(driver, FacebookMainFeed.class);
    }

    @AfterClass(alwaysRun = true)
    public void teardown(){
        driver.quit();
    }


    @Test(groups={"p1", "pageLoads"})
    public void loadPage(){
        fbMainPage.loadPage();
    }

    @Test(groups={"p2", "field"}, dependsOnMethods = "loadPage")
    public void filloutEmailFld(){
        fbMainPage.setText_EmailLogin("test.email@test.com");
    }

    @Test(groups={"p2", "field"}, dependsOnMethods = "filloutEmailFld")
    public void filloutPassFld(){
        fbMainPage.setText_PasswordLogin("123456789");
    }

    @Test(groups = {"p1"}, dataProviderClass = FacebookData.class, dataProvider = "login")
    public void TestLoginMainPage(String email, String password, String errorType){
        driver.manage().deleteAllCookies();
        fbMainPage.loadPage();
        fbMainPage.login(email, password);

        if (errorType != null){
            boolean result = fbLoginPage.checkErrorBox(errorType);
            assertTrue(result, "Expected error: "+errorType);
        }else{
            assertTrue(!fbMainFeed.getLinkText().isEmpty());
        }

    }

}
