package com.facebook.tests;

import com.roksi.data.FacebookData;
import com.roksi.pages.FacebookMainPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import static com.roksi.utilities.DriverFactory.*;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by roksolana.desyatnyk on 10/20/2016.
 */
public class SignUp_Flow_Test {
    public WebDriver driver;
    public WebDriverWait wait;
    FacebookMainPage fbMainPage;

    HashMap<String, String> signUpMap;

    @Factory(dataProvider = "signup", dataProviderClass = FacebookData.class)
    public SignUp_Flow_Test(String first, String last, String email){
        signUpMap = new HashMap<>();
        signUpMap.put("firstname", first);
        signUpMap.put("lastname", last);
        signUpMap.put("email", email);
    }

    @BeforeClass(alwaysRun = true)
    public void setup(){
        this.driver = getDriver(getBrowserTypeByProperty());
        wait = new WebDriverWait(driver, 5);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        fbMainPage = PageFactory.initElements(driver, FacebookMainPage.class);
    }

    @AfterClass(alwaysRun = true)
    public void teardown(){
        this.driver.quit();
    }

    @Test
    public void loadSignUpMainPage(){
        fbMainPage.loadPage();
        driver.manage().deleteAllCookies();
    }

    @Test(dependsOnMethods = "loadSignUpMainPage")
    public void signUpMainPageFirstNameField(){
        fbMainPage.setText_FirstNameField(signUpMap.get("firstname"));
    }
    @Test(dependsOnMethods = "signUpMainPageFirstNameField")
    public void signUpMainPageLastNameField(){
        fbMainPage.setText_LasNameField(signUpMap.get("lastname"));
    }
    @Test(dependsOnMethods = "signUpMainPageLastNameField")
    public void signUpMainPageEmailMobileField(){
        fbMainPage.setText_EmailMobileField(signUpMap.get("email"));
    }
}
