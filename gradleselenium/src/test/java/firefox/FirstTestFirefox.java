package firefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;


/**
 * Created by roksolana.desyatnyk on 9/13/2016.
 */
public class FirstTestFirefox {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "D:\\Selenium\\FirefoxDriver\\geckodriver.exe");
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();

        FirefoxOptions ffopt = new FirefoxOptions();
        ffopt.setBinary("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");

        WebDriver driver = new FirefoxDriver(capabilities);
        driver.get("http://www.justanswer.com/");
        WebElement inbox = driver.findElement(By.cssSelector("#questioninput"));
        inbox.sendKeys("How to learn automation testing");

        driver.close();
//        driver.quit();

    }
}