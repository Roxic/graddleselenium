package chrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Created by roksolana.desyatnyk on 9/13/2016.
 */
public class FirstTestChrome {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "D:\\Selenium\\ChromeDriver\\chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-maximized");
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.get("https://www.facebook.com/");
        WebElement email = driver.findElement(By.cssSelector("#email"));
        email.sendKeys("My_email");
        WebElement password = driver.findElement(By.cssSelector("#pass"));
        password.sendKeys("My_password");
//        driver.close();
//        driver.quit();
    }
}
