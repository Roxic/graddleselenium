package com.roksi.pages;

import com.roksi.BasePage;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roksolana.desyatnyk on 10/19/2016.
 */
public class FacebookLoginPage extends BasePage{

    @FindBy(css = "._53ij")    WebElement errorBox;

    public FacebookLoginPage(WebDriver driver) {
        super(driver);
        this.PAGE_TITLE = "Facebook";
        this.PAGE_URL = "https://www.facebook.com/login.php";
    }

    public boolean checkErrorBox(String text){
        try {
            return errorBox.getText().equals(text);
        }catch (NoSuchElementException e){
            return false;
        }
    }
}
