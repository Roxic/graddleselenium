package com.roksi.pages;

import com.roksi.BasePage;
import com.roksi.utilities.MyWaits;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by roksolana.desyatnyk on 10/18/2016.
 */
public class FacebookMainPage extends BasePage{

    @FindBy(id = "email")                       WebElement field_EmailLogin;
    @FindBy(id = "pass")                        WebElement field_PasswordLogin;
    @FindBy(id = "loginbutton")                 WebElement button_Login;
    @FindBy(name = "firstname")                 WebElement field_FirstNameSignUp;
    @FindBy(name = "lastname")                  WebElement field_LastNameSingUp;
    @FindBy(name = "reg_email__")               WebElement field_EmailMobileSignUp;
    @FindBy(name = "reg_email_confirmation__")  WebElement field_ReenterEmailMobileSignUp;
    @FindBy(name = "reg_passwd__")              WebElement field_PasswordSignUp;
    @FindBy(css = "#month")                     WebElement dropdown_Month;
    @FindBy(css = "#day")                       WebElement dropdown_Day;
    @FindBy(css = "#year")                      WebElement dropdown_Year;
    @FindBy(css = "span+span input[name='sex']")WebElement radio_Male;
    @FindBy(css = "button[name='websubmit']")   WebElement button_SignUp;


    public FacebookMainPage(WebDriver driver){
        super(driver);
        this.PAGE_TITLE = "Facebook - Log In or Sign Up";
        this.PAGE_URL = "https://www.facebook.com";
    }

    public void login(String email, String password){
        wait.until(MyWaits.visibilityOfElement(field_EmailLogin));
        wait.until(ExpectedConditions.elementToBeClickable(field_EmailLogin));
        setText_EmailLogin(email);
        setText_PasswordLogin(password);
        clickLoginMain();
    }

    public void setText_EmailLogin(String text){
        setElementText(field_EmailLogin, text);
    }
    public void setText_PasswordLogin(String text){
        setElementText(field_PasswordLogin, text);
    }

    public void clickLoginMain(){
        clickElement(button_Login);
    }

    public void setText_FirstNameField(String text){
        setElementText(field_FirstNameSignUp, text);
    }
    public void setText_LasNameField(String text){
        setElementText(field_LastNameSingUp, text);
    }
    public void setText_EmailMobileField(String text){
        setElementText(field_EmailMobileSignUp, text);
    }
    public void setText_ReenterEmailMobileField(String text){
        setElementText(field_ReenterEmailMobileSignUp, text);
    }
    public void setText_PasswordField(String text){
        setElementText(field_PasswordSignUp, text);
    }
    public void selectBdayMonth(String value){
        selectValueInDropdown(dropdown_Month, value);
    }
    public void selectBdayDay(String value){
        selectValueInDropdown(dropdown_Day, value);
    }
    public void selectBdayYear(String value){
        selectValueInDropdown(dropdown_Year, value);
    }
    public void click_MaleRadio(){
        clickElement(radio_Male);
    }
    public void click_SignUpButton(){
        clickElement(button_SignUp);
    }
}
