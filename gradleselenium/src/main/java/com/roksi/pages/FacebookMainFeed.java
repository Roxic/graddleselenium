package com.roksi.pages;

import com.roksi.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roksolana.desyatnyk on 10/19/2016.
 */
public class FacebookMainFeed extends BasePage{
    @FindBy(css = "#u_0_3")
    WebElement link_Home;

    public FacebookMainFeed(WebDriver driver) {
        super(driver);
    }

    public String  getLinkText(){
        return link_Home.getText();
    }
}
