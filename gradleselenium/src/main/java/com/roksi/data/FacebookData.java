package com.roksi.data;

import org.testng.annotations.DataProvider;

/**
 * Created by roksolana.desyatnyk on 10/13/2016.
 */
public class FacebookData {

    // args: URL, TITLE OF PAGE
    @DataProvider(name = "pages")
    public static Object[][] pages(){
        return new Object[][]{
                {"https://www.facebook.com", "Facebook - Log In or Sign Up"},
                {"https://www.google.com", "Google"},
                {"https://www.yahoo.com", "Yahoo"}
        };
    }

    // args: USERNAME, PASSWORD, ERROR_TITLE (null if no error expected)
    @DataProvider(name = "login")
    public static Object[][] login(){
        return new Object[][]{
                {"rdesyatnykgl@gmail.com", "1penguiniy?pass", null},
                {"abc", "123456789", "The email or phone number you’ve entered doesn’t match any account. Sign up for an account."}
        };
    }

    @DataProvider(name = "signup")
    public static Object[][] signup(){
        return new Object[][]{
                {"QA", "Roksi", "penguiniy1@mailinator.com"},
                {"$@^%$^%", ")(#%&)*", "penguiniy1@mailinator.com"},
                {"1233534", "3463546235", "penguiniy1@mailinator.com"}
        };
    }
}
