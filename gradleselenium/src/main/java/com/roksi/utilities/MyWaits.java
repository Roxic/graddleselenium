package com.roksi.utilities;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

/**
 * Created by roksolana.desyatnyk on 11/14/2016.
 */
public class MyWaits {

    public static ExpectedCondition<Boolean> visibilityOfElement(final WebElement element){
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver input) {
                try {
                    return element.isDisplayed();
                } catch (NoSuchElementException e){
                    return false;
                } catch (StaleElementReferenceException e1){
                    return false;
                }
            }
        };
    }

}
