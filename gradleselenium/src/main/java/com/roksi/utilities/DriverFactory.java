package com.roksi.utilities;

import junitx.util.PropertyManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

//import static com.roksi.utilities.DriverFactory.BrowserType.*;

/**
 * Created by roksolana.desyatnyk on 10/26/2016.
 */
public class DriverFactory {

    public enum BrowserType {
        FIREFOX("firefox"),
        CHROME("chrome"),
        IE("internet_explorer"),
        SAFARI("safari");

        public String value;

        BrowserType(String value) {
            this.value = value;
        }

        public String getBrowserName(){
            return this.value;
        }
    }

     public static WebDriver getDriver(BrowserType type){
         WebDriver driver;
         switch (type){
             case FIREFOX:
                 System.setProperty("webdriver.gecko.driver", "D:\\Selenium\\FirefoxDriver\\geckodriver.exe");
                 DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                 capabilities.setCapability("marionette", true);
                 driver = new FirefoxDriver(capabilities);
                 break;
             case CHROME:
                 System.setProperty("webdriver.chrome.driver", "D:\\Selenium\\ChromeDriver\\chromedriver.exe");
                 ChromeOptions chromeOptions = new ChromeOptions();
                 chromeOptions.addArguments("--start-maximized");
                 driver = new ChromeDriver(chromeOptions);
                 break;
             default:
                 System.setProperty("webdriver.chrome.driver", "D:\\Selenium\\ChromeDriver\\chromedriver.exe");
                 chromeOptions = new ChromeOptions();
                 chromeOptions.addArguments("--start-maximized");
                 driver = new ChromeDriver(chromeOptions);
                 break;
         }
         return driver;
     }

     public static BrowserType getBrowserTypeByProperty(){

         BrowserType type = null;
         String browsername = PropertyManager.getProperty("BROWSER");
         for (BrowserType bType : BrowserType.values()){
             if (bType.getBrowserName().equalsIgnoreCase(browsername)){
                 type = bType;
//                 System.out.println("BROWSER = " + type.getBrowserName());
             }
         }
         return type;
     }
}
