package com.kayak.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by roksolana.desyatnyk on 12/2/2016.
 */
public class SearchCity {
    public WebDriver driver;
    String url = "https://www.kayak.com/";
    String city = "Barcelona";

    @BeforeClass(alwaysRun = true)
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "D:\\Selenium\\ChromeDriver\\chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-maximized");
        this.driver = new ChromeDriver(chromeOptions);
    }

    @AfterClass(alwaysRun = true)
    public void teardown(){
        this.driver.quit();
    }

    @Test
    public void enterCityTest(){
        driver.manage().deleteAllCookies();
        driver.get(url);
        WebElement whereTextBox = driver.findElement(By.cssSelector("div[class='locationField col'] > input[id$='-location']"));
        whereTextBox.sendKeys(city);
        WebElement checkinDate = driver.findElement(By.cssSelector("div[class='startDateField col-1-2 col-1-4-m col-1-8-l '] > div[id$='-stayDates'] > div[id$='-stayDates-start-wrapper'] > span[id$='-stayDates-start'] > span[id$='-stayDates-start-placeholder'"));
        checkinDate.click();
        WebElement todayDate = driver.findElement(By.cssSelector("span[class='r9-datepicker-item r9-datepicker-enabled r9-datepicker-today']"));
        todayDate.click();
        WebElement searchButton = driver.findElement(By.cssSelector("div[class='searchBtn col col-1-2-s col-1-4-m col-1-8-l '] > button[id$='-submit']"));
        searchButton.click();
    }
}
